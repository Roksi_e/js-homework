// Создай класс, который будет создавать пользователей с именем и фамилией. Добавить к классу метод для вывода имени и фамилии

class Users {
    constructor(firstName, lastName){
        this.firstName = firstName;
        this.lastname = lastName
    };
    show (){
        console.log(`${this.firstName} ${this.lastname}`)
    }
}

const user1 = new Users('Ivan', 'Ivanov')
user1.show();


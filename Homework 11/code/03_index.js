// Создай поле для ввода цвета, когда пользователь выберет какой-то цвет сделай его фоном body

const btnColor = document.createElement('button')
    btnColor.textContent = 'Обрати'
    document.body.prepend(btnColor)

const inpColor = document.createElement('input')
    inpColor.type = 'color'
    inpColor.classList.add('color')
    document.body.prepend(inpColor)
    inpColor.placeholder = ('Введіть колір')

let userColor = document.querySelector('input')

btnColor.addEventListener('click', event =>{
    changeColor()
})

function changeColor (){
    document.body.style.background = userColor.value
}

// Создай инпут для ввода логина, когда пользователь начнет Вводить данные в инпут выводи их в консоль

const userLogin = document.createElement('input')
     userLogin.type = 'text'
     userLogin.placeholder = 'Введіть логін'
     userLogin.classList.add('login')
     document.body.append(userLogin)

userLogin.addEventListener('input', event =>{
    console.log(userLogin.value)
}) 

// Создайте поле для ввода данных поле введения данных выведите текст под полем

const infoUser = document.createElement('textarea')
      infoUser.placeholder = 'Напишіть щось'
      span = document.createElement('span')
      userLogin.after(infoUser)
      document.body.append(span)

infoUser.addEventListener('input', event => {
     span.innerText = event.target.value
})
const main = document.querySelector('.main')
const container = document.querySelector('.container')
const btn = document.querySelector('.btn')
const slide = document.querySelectorAll('.img')
const slides = container.querySelectorAll('div').length

let count = 0
let width

window.addEventListener('load', event =>{
    init()
    setInterval(shiftSlide, 3000)
})

function init (){
    width = main.clientWidth
    container.style.width = `${width * slides}px`
    slide.forEach(item => {
        item.style.width = `${width}px`
        item.style.height = 'auto'
    })
    shiftSlide()
}

function shiftSlide(){
    count++
    if(count >= slides){
        count = 0
    }
    container.style.transform = `translateX(-${count * width}px)`
}







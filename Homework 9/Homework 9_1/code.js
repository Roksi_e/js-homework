const container = document.querySelector('.container-stopwatch')
const startTime = document.querySelector('.start')
const stopTime = document.querySelector('.stop')
const resetTime = document.querySelector('.reset')
const minuteEl = document.querySelector('.min')
const secondEl = document.querySelector('.sec')
const millisecondEl = document.querySelector('.ms')

let millisecond = 0,
    second = 0,
    minute = 0,
    interval  

startTime.addEventListener('click', event =>{
    clearInterval(interval)
   
    interval = setInterval(timeStart, 10) 

    container.classList.add('green')
    container.classList.remove('red') 
    container.classList.remove('silver') 
    container.classList.remove('black') 
})

stopTime.addEventListener('click', event =>{
    clearInterval(interval)
    container.classList.add('red') 
    container.classList.remove('green') 
    container.classList.remove('silver') 
    container.classList.remove('black')
} )

resetTime.addEventListener('click', event =>{
    clearInterval(interval)
    minute = 0
    millisecond = 0
    second = 0
    minuteEl.innerText = `0${minute}`
    secondEl.innerText = `0${second}`
    millisecondEl.innerText = `0${millisecond}`
    container.classList.add('silver') 
    container.classList.remove('red') 
    container.classList.remove('green') 
    container.classList.remove('black')
} )

function timeStart (){   
    millisecond++
    if(millisecond <= 9){        
        millisecondEl.innerText = `0${millisecond}`
    }
    if(millisecond >= 10){
        millisecondEl.innerText = millisecond
    }
    if(millisecond > 99){        
        millisecond = 0       
        millisecondEl.innerText = `0${millisecond}`  
        second++    
        secondEl.innerText = `0${second}`
    }   
      
    if(second <= 9){          
       secondEl.innerText = `0${second}`
    }
    if(second >= 10){
        secondEl.innerText = second
    }
    if(second > 59){
        second = 0        
        secondEl.innerText = `0${second}` 
        minute++     
        minuteEl.innerText = `0${minute}`
    }
       
    if(minute <= 9){            
       minuteEl.innerText = `0${minute}`
    }
    if(minute >= 10){
        minuteEl.innerText = minute
    }
    if(minute > 59){
        minute = 0
        millisecond = 0
        second = 0
        minuteEl.innerText = `0${minute}`
        secondEl.innerText = `0${second}`
        millisecondEl.innerText = `0${millisecond}`
    }
}  


//--------------------------------------------------------------------------

    



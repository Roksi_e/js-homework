window.addEventListener("DOMContentLoaded",()=>{
const btn = document.querySelector(".keys"),
      display = document.querySelector(".display > input");
      results = document.querySelector('.orange')

const numbers = [...document.querySelectorAll('.black')].map(function (element) {
    return element.value
})
const signBtn = [...document.querySelectorAll('.pink')].map(function (element) {
    return element.value
})
const [...memoryBtn] = document.querySelectorAll('.gray')
const [...btnC] = numbers.pop()

const calc = {
    value1 : "",
    value2 : "",
    sign : "",
    finish : false,
    result : "",
    memory : "" 
}    
 
btn.addEventListener('click', (e) => { 
    key = e.target.value   

    if (!e.target.classList.contains('button')) return 
    if ( btnC.includes(key)) {
        clearAll()
        return
    }        

    if (numbers.includes(key)){
        if(calc.value2 === '' && calc.sign === ''){
            calc.value1 += key            
            show(calc.value1, display)
        }
        else  if (calc.value1 !== '' && calc.value2 !== '' && calc.finish   ) {             
            calc.finish = false
             calc.value1 =  calc.result
             calc.value2 = key
             show(calc.value2, display)                        
         }               
        else {
            calc.value2 += key            
            show(calc.value2, display)
        }
       return
    }

    if(signBtn.includes(key)){       
        calc.sign = key 
        if(calc.value2 == '') {
            show(calc.value1, display)
        }  
        else {
            show( calc.result, display)
        }       
       return
     }

    if(e.target.classList.contains('gray')){
      display.placeholder = e.target.value 
      calc.memory = calc.result
       memoryAll()
     }
     
   memoryBtn[0].addEventListener('click', event =>{
    if( calc.memory == ''){
        calc.memory = calc.result
       memoryAll()
                 
        }else{
            clearAll()           
         }  
   })
 
  show(calc.result, display) 

})

results.addEventListener('click', event =>{
    calculate()
    show(calc.result, display) 
}) 

function memoryAll (){      
    switch(calc.memory){
        case 'mrc' :   show(calc.result, display); break
        case 'm+' :   ( calc.result == '') ? (parseFloat(calc.value2) + calc.memory) : show(e.target.value, display); break
        case 'm-' :   ( calc.result == '') ? (parseFloat(calc.value2) - calc.memory) : show(e.target.value, display); break
    }
}

function calculate (){    
     switch (calc.sign) {            
           case "+" : calc.result =  (parseFloat(calc.value1) + parseFloat(calc.value2)) ;     
           break
           case "-" : calc.result = parseFloat(calc.value1) - parseFloat(calc.value2);
           break
           case "*" : calc.result = parseFloat(calc.value1) * parseFloat(calc.value2);
           break
           case "/" :
               if( calc.value2 == 0) {
                calc.value2 = ''
                calc.sign = ''
                calc.value1 = ''
                calc.result = 'Error'
                return show(calc.result, display)

               } else {  calc.result = parseFloat(calc.value1) / parseFloat(calc.value2);} 
           break          
        }
        
        calc.finish = true
   return  (calc.result)   
   }

function clearAll () {
    calc.value1 = ''
    calc.value2 = ''
    calc.sign = ''
    calc.result = ''
   
    show('0', display)
}

function show(value, el) {
    el.value = value
}

})

const data =  fetch("https://swapi.dev/api/people/")

data
.then((item)=>{ return item.json() })
.then((data1)=>{
        showHero(data1.results)
})
.catch((e)=>{console.error(e)})

function showHero(arr){    
    arr.forEach(element => {
        const {name, gender, birth_year,homeworld} = element

        const cardHero = document.createElement('div')
        const nameHero = document.createElement('p')
        const genderHero = document.createElement('p')
        const birthHero = document.createElement('p')
        const homeHero = document.createElement('p')

        nameHero.textContent = `Name: ${name}`
        genderHero.textContent = `Gender: ${gender}`
        birthHero.textContent = `Birth: ${birth_year}`
        

        const data3 = fetch(homeworld)
        data3.then((item) =>{return item.json()})
        .then((planet) => {
        homeHero.textContent = `Planet: ${planet.name}`        
        })

        cardHero.append(nameHero, genderHero, birthHero, homeHero)
        document.querySelector('.main').append(cardHero)

        randomColor(cardHero)

    }); 
}

function randomColor(element) {
    let color =  `hsl(${Math.floor(Math.random() * 360)}, 80%, 50%)`
    element.style.backgroundColor = color
}
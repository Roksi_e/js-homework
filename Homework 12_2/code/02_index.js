const banner = document.querySelector('#banner')
const bannerBtn = document.querySelector('#banner > button')

const random = (min, max) =>{
    return Math.floor(Math.random() * (max - min + 1) + min)
}

bannerBtn.addEventListener('click', event =>{
    banner.style.position = 'absolute'
    bannerBtn.style.cursor = 'pointer'
 
     banner.style.top = `${random(0, 80)}%`
     banner.style.left = `${random(0,80)}%`
})

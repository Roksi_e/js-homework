window.addEventListener('DOMContentLoaded', function(){

const pizzaTable = document.querySelector('.table')
const [...ingridients] = document.querySelectorAll('.draggable')
const [...resultAll] = document.querySelectorAll('.result > div')
const [...radioBtns] = document.querySelectorAll('.radio')
let addit = document.querySelector('.ingridients')

let span = ''
let sauceResCount = 0
let topingResCount = 0
let price = 0
let priseAll = 0
let key = ''

resultAll.forEach(element =>{
    span = document.createElement('span')
    span.classList.add('span')
    element.appendChild(span)    
})

const saucesResult = document. querySelector('.sauces > span')
const priceResult = document.querySelector('.price > span')
const topingsResult = document.querySelector('.topings > span')

const pissaPrice = {
    small : '80',
    mid : '150',
    big : '180',
    sauceClassic : '20',
    sauceBBQ : '25',
    sauceRikotta : '30',
    cheese : '50',
    feta : '60',
    mocarela : '70',
    beef : '80',
    tomato : '50',
    mushrooms : '60'
}

radioBtns.forEach(element =>{
    element.addEventListener('click', e =>{        
        if(e.target.value === 'small'){
            price = pissaPrice.small
        }
        if (e.target.value === 'mid'){
            price = pissaPrice.mid
          }
          if(e.target.value === 'big'){
            price = pissaPrice.big
          }    
        priceResult.innerText = `${price} грн`       
    })    
})

addit.addEventListener('mousedown', e =>{
    key = e.target.id
})

ingridients.forEach(el => {    
    el.addEventListener('dragstart', function(evt){
        this.style.border = "3px dotted #000";
        evt.dataTransfer.effectAllowed = "move";
    },
    false)
    
    el.addEventListener('dragend', function(evt){
        this.style.border = "none";
    },
    false)
})

pizzaTable.addEventListener('dragover', event =>{
    event.preventDefault()
})

pizzaTable.addEventListener('dragenter', function(evt){
    pizzaTable.style.border = "3px solid red"

},
false)

pizzaTable.addEventListener('drop', function(evt){    
    if(evt.preventDefault) evt.preventDefault()
    if(evt.stopPropagation) evt.stopPropagation()

    pizzaTable.style.border = ""

    let div = document.createElement('img')
    div.className = 'sauce'
    pizzaTable.appendChild(div)   
   
         if(key === 'sauceClassic'){       
           div.src = "./img/sous-klassicheskij_1557758736353.png"
           saucesResult.innerText = `${(sauceResCount += parseInt(pissaPrice.sauceClassic))} грн`
        }
     
       if(key === 'sauceBBQ'){   
           div.src = "./img/sous-bbq_155679418013.png"         
           saucesResult.innerText = `${(sauceResCount += parseInt(pissaPrice.sauceBBQ))} грн`     
       }
    
        if(key === 'sauceRikotta'){       
            div.src = "./img/sous-rikotta_1556623391103.png"        
            saucesResult.innerText = `${(sauceResCount += parseInt(pissaPrice.sauceRikotta))} грн`              
        }
    
        if(key === 'moc1'){       
            div.src = "./img/mocarela_1556623220308.png"        
            topingsResult.innerText = `${(topingResCount += parseInt(pissaPrice.cheese))} грн`              
        }
    
        if(key === 'moc2'){       
            div.src = "./img/mocarela_1556785182818.png"        
            topingsResult.innerText = `${(topingResCount += parseInt(pissaPrice.feta))} грн`              
        }
    
        if(key === 'moc3'){       
            div.src = "./img/mocarela_1556785198489.png"        
            topingsResult.innerText = `${(topingResCount += parseInt(pissaPrice.mocarela))} грн`              
        }
    
        if(key === "telya"){       
            div.src = "./img/telyatina_1556624025747.png"        
            topingsResult.innerText = `${(topingResCount += parseInt(pissaPrice.beef))} грн`              
        }
    
        if(key === "vetch1"){       
            div.src = "./img/vetchina.png"        
            topingsResult.innerText = `${(topingResCount += parseInt(pissaPrice.tomato))} грн`              
        }
    
        if(key === "vetch2"){       
            div.src = "./img/vetchina_1556623556129.png"        
            topingsResult.innerText = `${(topingResCount += parseInt(pissaPrice.mushrooms))} грн`              
        }
    
    priceResult.innerText = `${(priseAll = ((+price) + (+topingResCount) + (+sauceResCount)))} грн`

    return false
}, 
false)

})


const [...f] = document.info

const customerInfo = f.map((element) =>{
    return element
}).filter((element) =>{
    return element.className != 'button'
})
let res
customerInfo.forEach((el) => {
    el.addEventListener('change', event =>{
        validate(event.target)
        if(res == true){
            event.target.classList.add('success')
            event.target.classList.remove('error')
        }
        else {
            event.target.classList.add('error')
            event.target.classList.remove('success')
        }      
    })
});

function validate(target) {    
   switch(target.type) {
    case 'text' : return  res = /^[A-zА-яієї]{2,}$/i.test(target.value);
    case 'tel' : return  res = /^\+380\d{9}$/.test(target.value);
    case 'email' : return  res = /\b[A-z0-9.-_]+@[a-z0-9.-]+\.[a-z]{2,4}\b/i.test(target.value)   
   }
}

f[3].addEventListener('click', event =>{
        confirm('Ви впевненні?') 
        customerInfo.forEach((el) =>{
           el.target = '' 
           event.target.classList.remove('error')   
           event.target.classList.remove('success')       
        }) 
        
              
 })

 f[4].addEventListener('click', event =>{
    if(res == true){
      document.location.assign('./thank-you.html')  
    }
    else {
        alert('Заповніть форму')
    }
    
 })

